param (
    [string]$jsonFilePath,
    [string]$xmlFilePath
)

function Update-Placeholders ($node, $jsonData) {
    foreach ($child in $node.ChildNodes) {
        if ($child -is [System.Xml.XmlText]) {
            $placeholderPattern = "{(.+?)}"
            if ($child.Value -match $placeholderPattern) {
                $placeholder = $matches[1]
                if ($jsonData.PSObject.Properties[$placeholder]) {
                    $child.Value = $jsonData.PSObject.Properties[$placeholder].Value
                }
            }
        } elseif ($child.HasChildNodes) {
            Update-Placeholders -node $child -jsonData $jsonData
        }
    }
}

if (-Not (Test-Path -Path $jsonFilePath)) {
    Write-Error "JSON file not found: $jsonFilePath"
    exit 1
}

if (-Not (Test-Path -Path $xmlFilePath)) {
    Write-Error "XML file not found: $xmlFilePath"
    exit 1
}

$jsonContent = Get-Content -Path $jsonFilePath -Raw | ConvertFrom-Json
[xml]$xmlContent = Get-Content -Path $xmlFilePath

Update-Placeholders -node $xmlContent -jsonData $jsonContent
$xmlContent.Save($xmlFilePath)

Write-Output "XML file has been successfully updated with values from JSON."